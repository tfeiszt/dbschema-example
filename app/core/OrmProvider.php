<?php
namespace App\core;

use App\model\Schema;
use Psr\Container\ContainerInterface;

/**
 * Class OrmProvider
 * @package App
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class OrmProvider
{
    /**
     * @param ContainerInterface $container
     * @param array $args
     * @return void
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function registerModels(ContainerInterface $container, $args = [])
    {
        list($pdo) = $args;
        foreach ($this->getModelList() as $key => $className) {
            $container[$key] = function() use ($className, $pdo){
                return new $className($pdo);
            };
        }
        return;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function getModelList()
    {
        $classNames = [
            'Schema' => Schema::class
        ];
        foreach (Schema::getRegisteredTables() as $className) {
            $classNames[(new \ReflectionClass($className))->getShortName()] = $className;
        }
        return $classNames;
    }
}
