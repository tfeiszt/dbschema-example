<?php
namespace App\core;

use Psr\Container\ContainerInterface;

/**
 * Class Container
 * @package App
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class Container extends \ArrayObject implements ContainerInterface
{
    /**
     * @param $key
     * @return bool
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function has($key)
    {
        return isset($this[$key]);
    }

    /**
     * @param $key
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function get($key)
    {
        if (is_callable($this[$key])) {
            return $this[$key]();
        }
        return $this[$key];
    }

    /**
     * @param $name
     * @param $value
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function __set($name, $value)
    {
        $this[$name] = $value;
    }

    /**
     * @param $name
     * @return mixed
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public function __get($name)
    {
        return $this->get($name);
    }
}
