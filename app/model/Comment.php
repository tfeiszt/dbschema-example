<?php
namespace App\model;

use tfeiszt\DbSchema\AbstractTableDefinition;
use tfeiszt\DbSchema\Enum\AbstractDataType;
use tfeiszt\DbSchema\Enum\AbstractSchemaType;

/**
 * Class Comment
 * @package App\model
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class Comment extends AbstractTableDefinition
{
    /**
     * Registered fields
     * Mapping of table or a view.
     * Field names are camelCased.
     * [
     *      'id' => AbstractDataType::INT_PRIMARY_KEY,
     *      'post' => 'App\model\Post',
     *      'user' => 'App\model\User',
     *      'content' => AbstractDataType::STRING,
     *      'publishedAt' => AbstractDataType::TIMESTAMP
     * ]
     *
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getMapping()
    {
        return [
            'id' => AbstractDataType::INT_PRIMARY_KEY,
            'post' => 'App\model\Post',
            'user' => 'App\model\User',
            'content' => AbstractDataType::TEXT,
            'publishedAt' => AbstractDataType::TIMESTAMP
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function hasMany()
    {
        return [];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function manyToMany()
    {
        return [];
    }

    /**
     * Post and User is indexed
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getIndices()
    {
        return [
            'idx_user' => 'user',
            'idx_post' => 'post'
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getDefaults()
    {
        return [];
    }

    /**
     * published_at is formatted to d/m/Y by default (field->getDisplayValue())
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getFormats()
    {
        return [
            'publishedAt' => 'd/m/Y'
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getEnumValues()
    {
        return [];
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getTablePrefix()
    {
        return 'wp_';
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getPk()
    {
        return 'id';
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getSchemaType()
    {
        return AbstractSchemaType::MYSQL;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getDefaultOrderSet()
    {
        return [
            ['id', 'ASC']
        ];
    }

    /**
     * Overridden afterCreate
     *
     * @author Tamas Feiszt <tamas.feiszt@d3r.com>
     */
    public function afterCreate()
    {
        var_dump('afterCreate trigger has been triggered on model [' . $this->id . ']');
    }
}
