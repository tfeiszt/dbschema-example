<?php
namespace App\model;

use tfeiszt\DbSchema\AbstractTableDefinition;
use tfeiszt\DbSchema\Enum\AbstractDataType;
use tfeiszt\DbSchema\Enum\AbstractSchemaType;

/**
 * Class Post
 * @package App\model
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class Post extends AbstractTableDefinition
{
    /**
     * Registered fields
     * Mapping of table or a view.
     * Field names are camelCased.
     * [
     *      'id' => AbstractDataType::INT_PRIMARY_KEY,
     *      'user' => 'App\model\User',
     *      'title' => AbstractDataType::STRING,
     *      'slug' => AbstractDataType::STRING,
     *      'summary' => AbstractDataType::STRING,
     *      'content' => AbstractDataType::LONGTEXT,
     *      'publishedAt' => AbstractDataType::TIMESTAMP,
     *      'editedAt' => AbstractDataType::TIMESTAMP_ON_UPDATE
     * ]
     *
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getMapping()
    {
        return [
            'id' => AbstractDataType::INT_PRIMARY_KEY,
            'user' => 'App\model\User',
            'title' => AbstractDataType::STRING,
            'slug' => AbstractDataType::STRING,
            'summary' => AbstractDataType::STRING,
            'content' => AbstractDataType::LONGTEXT,
            'publishedAt' => AbstractDataType::TIMESTAMP,
            'editedAt' => AbstractDataType::TIMESTAMP_ON_UPDATE
        ];
    }

    /**
     * lint to table comments
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function hasMany()
    {
        return [
            'comments' => 'App\model\Comment[post]'
        ];
    }

    /**
     * Link to tags - posts relating table
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function manyToMany()
    {
        return [
            'tags' => 'App\model\Tag[]'
        ];
    }

    /**
     * User is indexed
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getIndices()
    {
        return [
            'idx_user' => 'user'
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getDefaults()
    {
        return [];
    }

    /**
     * published_at is formatted to d/m/Y by default (field->getDisplayValue())
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getFormats()
    {
        return [
            'publishedAt' => 'd/m/Y'
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getEnumValues()
    {
        return [];
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getTablePrefix()
    {
        return 'wp_';
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getPk()
    {
        return 'id';
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getSchemaType()
    {
        return AbstractSchemaType::MYSQL;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getDefaultOrderSet()
    {
        return [
            ['id', 'ASC']
        ];
    }
}
