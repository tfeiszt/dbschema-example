<?php
namespace App\model;

use tfeiszt\DbSchema\AbstractTableDefinition;
use tfeiszt\DbSchema\Enum\AbstractDataType;
use tfeiszt\DbSchema\Enum\AbstractSchemaType;

/**
 * Class User
 * @package App\model
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class User extends AbstractTableDefinition
{
    /**
     * Registered fields
     * Mapping of table or a view.
     * Field names are camelCased.
     * [
     *      'id' => AbstractDataType::INT_PRIMARY_KEY,
     *      'urlName' => AbstractDataType::CHAR_50,
     *      'name' => AbstractDataType::STRING,
     *      'textDescription' => AbstractDataType::TEXT,
     *      'userRole' => AbstractDataType::SIMPLE_ENUM,
     *      'userActive' => AbstractDataType::BOOLEAN_ENUM,
     *      'createdDate' => AbstractDataType::DATETIME
     * ]
     *
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getMapping()
    {
        return [
            'id' => AbstractDataType::INT_PRIMARY_KEY,
            'fullName' => AbstractDataType::STRING,
            'username' =>AbstractDataType::CHAR_50,
            'email'=> AbstractDataType::STRING,
            'password'=> AbstractDataType::STRING,
            'roles'=> AbstractDataType::JSON,
        ];
    }

    /**
     * One to many relations to another entity class
     * [
     *      'address' => '[Address[]]'
     * ]
     *
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function hasMany()
    {
        return [
            'posts' => 'App\model\Post[user]',
            'comments' => 'App\model\Comment[user]'
        ];
    }

    /**
     * Many to many relations to another entity class
     * [
     *      'tag' => 'Tag[]'
     * ]
     *
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
     public static function manyToMany()
     {
         return [];
     }

    /**
     * Mapping indexed fields. Not included primary key!
     * Index id is under_score_named, field name is camelCased
     * [
     *      'idx_url_name' => 'urlName',
     *      'unique_idx_username => 'userName' // For UNIQUE index
     * ]
     *
     *
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getIndices()
    {
        return [
            'unique_idx_email' => 'email',
            'unique_idx_username' => 'username'
        ];
    }

    /**
     * Mapping of fields and default values.
     * It can be value or closure.
     *
     * [
     *      'userActive' => 1,
     *      'createdDate' => function() {
     *          return Date('Y-m-d H:i:s');
     *      },
     * ]
     *
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getDefaults()
    {
        return [
            'roles' => ['ROLE_USER']
        ];
    }

    /**
     * Mapping of fields and default display formats
     * [
     *      'createdDate' => 'd/m/Y'
     * ]
     *
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getFormats()
    {
        return [];
    }

    /**
     * Values for enumeration field types.
     * [
     *      'userActive' => [
     *          'yes' => true,
     *          'no' => false
     *      ],
     *      'userRole' => [
     *          'user' => 'User',
     *          'admin' => 'Administrator',
     *          'super' => 'Super Admin'
     *      ]
     * ]
     *
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getEnumValues()
    {
        return [];
    }

    /**
     * Returns table prefix if it has one, or empty string
     *
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getTablePrefix()
    {
        return 'wp_';
    }

    /**
     * Returns PK field camelCased name
     *
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getPk()
    {
        return 'id';
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getSchemaType()
    {
        return AbstractSchemaType::MYSQL;
    }

    public static function getDefaultOrderSet()
    {
        return [
            ['id', 'ASC']
        ];
    }
}