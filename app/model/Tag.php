<?php
namespace App\model;

use tfeiszt\DbSchema\AbstractTableDefinition;
use tfeiszt\DbSchema\Enum\AbstractDataType;
use tfeiszt\DbSchema\Enum\AbstractSchemaType;

/**
 * Class Tag
 * @package App\model
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class Tag extends AbstractTableDefinition
{
    /**
     * Registered fields
     * Mapping of table or a view.
     * Field names are camelCased.
     * [
     *      'id' => AbstractDataType::INT_PRIMARY_KEY,
     *      'name' => AbstractDataType::STRING,
     * ]
     *
     * @return []
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getMapping()
    {
        return [
            'id' => AbstractDataType::INT_PRIMARY_KEY,
            'name' => AbstractDataType::STRING
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function hasMany()
    {
        return [];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function manyToMany()
    {
        return [];
    }

    /**
     * Tag name is unique
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getIndices()
    {
        return [
            'unique_idx_tag_name' => 'name',
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getDefaults()
    {
        return [
            'name' => ''
        ];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getFormats()
    {
        return [];
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getEnumValues()
    {
        return [];
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getTablePrefix()
    {
        return 'wp_';
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getPk()
    {
        return 'id';
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getSchemaType()
    {
        return AbstractSchemaType::MYSQL;
    }

    /**
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getDefaultOrderSet()
    {
        return [
            ['id', 'ASC']
        ];
    }
}
