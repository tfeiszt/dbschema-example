<?php
namespace App\model;

use tfeiszt\DbSchema\AbstractSchemaDefinition;
use tfeiszt\DbSchema\Enum\AbstractSchemaType;

/**
 * Class Schema
 * @author Tamas Feiszt <tfeiszt@gmail.com>
 */
class Schema extends AbstractSchemaDefinition
{
    /**
     * Registered tables
     * @return array
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getRegisteredTables()
    {
        return [
            \App\model\User::class,
            \App\model\Tag::class,
            \App\model\Post::class,
            \App\model\Comment::class
        ];
    }

    /**
     * @return string
     * @author Tamas Feiszt <tfeiszt@gmail.com>
     */
    public static function getSchemaType()
    {
        return AbstractSchemaType::MYSQL;
    }
}