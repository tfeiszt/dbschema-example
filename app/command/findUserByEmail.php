<?php

require_once __DIR__. '/../../vendor/autoload.php';

global $argv;

$email = (string) (isset($argv[1]) ? $argv[1] : '');

if ($email == '--help') {
    echo "findUserByEmail [--help] [(string) email]";
    exit;
}

if (empty($email)) {
    echo "No Email provided!. Please provide an Email as first parameter.";
    exit;
}

// Creating Database connection
$config = require __DIR__ . '/../config/db.php';
$pdo = new \PDO( "mysql:host={$config['host']}; dbname={$config['name']}", $config['user'], $config['pwd']);

// Creating container
$container = new \App\core\Container();

// Registering ORM
$orm = new \App\core\OrmProvider();
$orm->registerModels($container, [$pdo]);

// Creating model
/** @var \App\model\User $model */
$model = $container->get('User');

// Finding the user by email using magic method
if ($user = $model->findFirstByEmail($email)) {
    echo "ID: " . $user->id . "\n";
    echo "Full name: " . $user->fullName . "\n";
    echo "Username: " . $user->username . "\n";
    echo "Email: " . $user->email . "\n";
    echo "Number of comments added: " .$user->comments()->count() . "\n";
    echo "Comments:\n"; // Listing comments of the user
    foreach ($user->comments as $comment) {
        echo $comment->publishedAt()->getDisplayValue() . " " . $comment->content . "\n\n";
    }
    echo "Number of posts written: " . count($user->posts);
}else {
    echo "Not found.\n";
}
