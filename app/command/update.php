<?php

require_once __DIR__. '/../../vendor/autoload.php';

$config = require __DIR__ . '/../config/db.php';
$pdo = new \PDO( "mysql:host={$config['host']}; dbname={$config['name']}", $config['user'], $config['pwd']);

// Creating a container
$container = new \App\core\Container();

// Registering ORM models on container (autoload)
$orm = new \App\core\OrmProvider();
$orm->registerModels($container, [$pdo]);

// Creating new DB schema instance
$schema = $container->get('Schema');

echo "Updating schema\n";
// Updating database. It creates new tables, fields, indices, drops old indices. It doesn't drop any unregistered old tables, removed fields.
$script = $schema->update();
echo $script->toSql() . "\n";
echo "Done\n";
