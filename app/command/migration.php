<?php

require_once __DIR__. '/../../vendor/autoload.php';

$config = require __DIR__ . '/../config/db.php';
$pdo = new \PDO( "mysql:host={$config['host']}; dbname={$config['name']}", $config['user'], $config['pwd']);

// Creating a container
$container = new \App\core\Container();

// Registering ORM models
$orm = new \App\core\OrmProvider();
$orm->registerModels($container, [$pdo]);

// Creating new DB schema instance
$schema = $container->get('Schema');

echo "Creating schema\n";
// Running an update on the schema instance. It creates tables, indices.
$script = $schema->update();
// Running data import script
$customStatements = new \tfeiszt\DbSchema\Model\DDLStatement('Inserting data', file_get_contents(__DIR__ . '/../resources/migration/data.sql'));
$customScript = new \tfeiszt\DbSchema\Model\DDLScript($pdo);
$customScript->runScript($customStatements);
// Migration and import done.
// Printing the sql script at the end.
echo $script->toSql() . "\n";
echo $customScript->toSql() . "\n";
echo "Done\n";