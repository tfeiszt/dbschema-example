<?php

require_once __DIR__. '/../../vendor/autoload.php';

global $argv;

$id = (isset($argv[1]) ? $argv[1] : '');
$userId = (int) (isset($argv[2]) ? $argv[2] : '');
$comment = (string) (isset($argv[3]) ? $argv[3] : '');

if ($id == '--help') {
    echo "addCommentToPost [--help] [(int) postID] [(int) userId] [(string) comment]";
    exit;
}

if (empty($id)) {
    echo "No ID provided!. Please provide an ID as first parameter.";
    exit;
}

if (empty($userId)) {
    echo "No user provided!. Please provide a user ID as second parameter.";
    exit;
}

if (empty($comment)) {
    echo "No comment text provided!. Please provide comment text as third parameter.";
    exit;
}

// Creating Database connection
$config = require __DIR__ . '/../config/db.php';
$pdo = new \PDO( "mysql:host={$config['host']}; dbname={$config['name']}", $config['user'], $config['pwd']);

// Creating container
$container = new \App\core\Container();

// Registering ORM
$orm = new \App\core\OrmProvider();
$orm->registerModels($container, [$pdo]);

// Creating model
/** @var \App\model\Post $model */
$model = $container->get('Post');

// Find the post by Id
if ($model->findOne($id) && $model->isLoaded()) {
    // Printing Post data
    echo "Selected post data:\n";
    echo "ID: ". $id . "\n";
    echo "Title: " . $model->title . "\n";
    echo "Author: " . $model->user->fullName . "\n";
    echo "Tags: \n";
    if ($model->tags()->count()) {
        foreach ($model->tags as $tag) {
            echo $tag->name . "\n";
        }
    }
    echo "\n";
    echo "Number of comments: " . $model->comments()->count() . "\n";
    echo "Adding a comment\n";
    $model->comments()->add()->setFormData([
        'user' => $userId,
        'content' => $comment,
        'publishedAt' => date('Y-m-d H:i:s')
    ]);
    $model->comments()->save();
    echo "Comment has been saved\n";
    echo "New number of comments: " . $model->comments()->count() . "\n";
    echo "Comments:\n";
    foreach ($model->comments as $k => $comment) {
        echo ($k + 1) .": \n";
        echo $comment->content . "\n";
    }
} else {
    echo "Post not found.\n";
}
