<?php

require_once __DIR__. '/../../vendor/autoload.php';

global $argv;

$str = (string) (isset($argv[1]) ? $argv[1] : '');

if ($str == '--help') {
    echo "findPostsByTag [--help] [(string) tag]";
    exit;
}

if (empty($str)) {
    echo "No tag provided!. Please provide an tag as first parameter.";
    exit;
}

// Creating Database connection
$config = require __DIR__ . '/../config/db.php';
$pdo = new \PDO( "mysql:host={$config['host']}; dbname={$config['name']}", $config['user'], $config['pwd']);

// Creating container
$container = new \App\core\Container();

// Registering ORM
$orm = new \App\core\OrmProvider();
$orm->registerModels($container, [$pdo]);

// Creating model
/** @var \App\model\Post $model */
$model = $container->get('Post');

// Creating custom select for the model
$select = $model->getSelect('wp_post.*', 'wp_tag.name'); // It points to the fact table
$select->join('wp_post_tag')->on('wp_post.id = wp_post_tag.post'); // Join to relation table
$select->join('wp_tag')->on('wp_post_tag.tag = wp_tag.id'); // Join to second fact table
$select->where()
    ->equal('wp_tag.name', $str);

if ($posts = $model->findAll($select)) { // Finding all posts by given query
    foreach ($posts as $post) {
        echo "ID: ". $post->id . "\n";
        echo "Title: " . $post->title . "\n";
        echo "Number of tags: " . $post->tags()->count() . "\n";
        if ($post->tags()->count()) { // Listing all tags related to the post
            echo "Tags: \n";
            foreach ($post->tags as $tag) {
                echo $tag->name . "\n";
            }
        }
        echo "\n";
    }
} else {
    echo "No post tagged with [" . $str . "]";
}
