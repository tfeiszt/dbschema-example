<?php

require_once __DIR__. '/../../vendor/autoload.php';

global $argv;

$id = (isset($argv[1]) ? $argv[1] : '');

$str = (string) (isset($argv[2]) ? $argv[2] : '');

if ($id == '--help') {
    echo "addTagToPost [--help] [(int) postId] [(string) tag]";
    exit;
}

if (empty($id)) {
    echo "No ID provided!. Please provide an ID as first parameter.";
    exit;
}

if (empty($str)) {
    echo "No tag provided!. Please provide an tag as second parameter.";
    exit;
}

// Creating Database connection
$config = require __DIR__ . '/../config/db.php';
$pdo = new \PDO( "mysql:host={$config['host']}; dbname={$config['name']}", $config['user'], $config['pwd']);

// Creating container
$container = new \App\core\Container();

// Registering ORM
$orm = new \App\core\OrmProvider();
$orm->registerModels($container, [$pdo]);

// Creating model
/** @var \App\model\Post $model */
if (!$model = $container->get('Post')->findOne($id)) {
    echo "Post not found.\n";
    exit;
}

// Creating an empty Tag model
$tag = $container->get('Tag');

// Checking is tag exist using magic method
if ($exists = $tag->findFirstByName($str)) {
    echo 'The given tag "' . $exists->name . '" is exists.';
    exit;
}

// Creating new instance from given data
$tag = \App\model\Tag::createFromData($pdo, ['name' => $str]);
$tag = $tag->save();
// Append it to the post instance
$model->tags()->append($tag);
$model->save();

// Creating custom select to pick the post by the new tag
$select = $model->getSelect('wp_post.*', 'wp_tag.name'); // It points to the fact table
$select->join('wp_post_tag')->on('wp_post.id = wp_post_tag.post'); // Join to relation table
$select->join('wp_tag')->on('wp_post_tag.tag = wp_tag.id'); // Join to second fact table
$select->where()
    ->equal('wp_tag.name', $str);

if ($posts = $model->findAll($select)) { // Finding by the given query object
    foreach ($posts as $post) {
        echo "ID: ". $post->id . "\n";
        echo "Title: " . $post->title . "\n";
        echo "Tags: \n";
        if ($tags = $post->tags()->getValue()) { // Listing all tags related to the post
            foreach ($tags as $tag) {
                echo $tag->name . "\n";
            }
        }
        echo "\n";
    }
}
