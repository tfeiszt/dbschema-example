<?php

require_once __DIR__. '/../../vendor/autoload.php';

global $argv;

$id = (int) (isset($argv[1]) ? $argv[1] : '');

if ($id == '--help') {
    echo "findPostById [--help] [(int) postID]";
    exit;
}

if (empty($id)) {
    echo "No ID provided!. Please provide an ID as first parameter.";
    exit;
}

// Creating Database connection
$config = require __DIR__ . '/../config/db.php';
$pdo = new \PDO( "mysql:host={$config['host']}; dbname={$config['name']}", $config['user'], $config['pwd']);

// Creating container
$container = new \App\core\Container();

// Registering ORM
$orm = new \App\core\OrmProvider();
$orm->registerModels($container, [$pdo]);

// Creating model
/** @var \App\model\Post $model */
$model = $container->get('Post');

// Finding the post by id
if ($model->findOne($id) && $model->isLoaded()) {
    // Printing Post data
    echo "Selected post data:\n";
    echo "ID: ". $id . "\n";
    echo "Title: " . $model->title . "\n";
    echo "Author: " . $model->user->fullName . "\n";
    echo "Number of comments: " . $model->comments()->count() . "\n";
    echo "Tags: \n";
    if ($model->tags()->count()) {
        foreach ($model->tags as $tag) {
            echo $tag->name . "\n";
        }
    }
    echo "\n";
    echo "Usage of related model:\n";
    echo '$model->user: ' . get_class($model->user) . " [User instance]\n";
    echo '$model->user->username: ' . $model->user->username . " [string value]\n";
    echo '$model->user(): ' . get_class($model->user()) . " [ModelField instance]\n";
    echo '$model->user()->getValue(): ' . get_class($model->user()->getValue()) . " [User instance]\n";
    echo '$model->user()->getRawValue(): ' . $model->user()->getRawValue() . " [integer]\n";
} else {
    echo "Post not found.\n";
}
