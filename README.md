# dbschema-example

### Description
Simple ORM database mapping demo program using tfeiszt/dbschema

### Installation
1. Clone / download this repo
2. Run composer install

### Migration
1. Create an empty mysql or mariadb database
2. Edit database name and connection credentials in configuration file: app/config/db.php
3. Open a console in folder app/command
4. Run the following command: ```php migration.php```

### Available commands
* Read a post by ID
```php findPostById [--help] [(int) postID]```
* Read all tagged posts by tag
```php findPostsByTag [--help] [(string) tag]```
* Find comments by post id
  ```php findCommentsByPostId [--help] [(int) postID]```
* Read a user by email
```php findUserByEmail [--help] [(string) email]```
* Find comments of a user by email (Using magic method instead of Many To Many field)
  ```php finndCommentsByUser [--help] [(string) email]```
* Adding a tag to post by post ID
```php addTagToPost [--help] [(int) postId] [(string) tag]```
* Adding a comment to a post by ID
```php addCommentToPost [--help] [(int) postID] [(int) userId] [(string) comment]```

### Updating database
You can update your database by changing model structure.
Available updates:

* Adding table: Create your new model
* Adding new field to an existing table: Add a new field to the mapping on the existing model. (method getMapping())
* Adding new index: add an index to your model (method getIndices())
* Adding relation table (hasMany() and manyToMany() methods) 
* Adding model dependency (method getMapping() using related model class instead of data type)

### Run updates
1. Do a change on an existing model, or make your own new one based on tfeiszt\DbSchema\AbstractTableDefinition.
2. Register it into app/model/Schema (See method getRegisteredTables())
3. Open a console in folder app/command
4. Run the following command: ```php update.php```

### License
MIT
